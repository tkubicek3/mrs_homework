import React, {Component} from "react";
import "../css/styles.css";
import {Button, Table} from "react-bootstrap";


/**
 * Component table of users with update and delete components.
 *
 * Created by Tomas Kubicek, 2016.
 */
export default class UsersTable extends Component {

    /**
     * @returns {XML} Table with users.
     */
    render() {
        return (
            <Table striped bordered condensed hover>
                <TableHead header={this.props.header}/>
                <TableBody header={this.props.header} data={this.props.data}
                           onRowDeleted={this.props.onRowDeleted} onRowUpdated={this.props.onRowUpdated}/>
            </Table>
        );
    }
}
// TODO: Specifikovat vice?
UsersTable.propTypes = {
    header: React.PropTypes.array.isRequired,
    data: React.PropTypes.array.isRequired,
    onRowDeleted: React.PropTypes.func.isRequired,
    onRowUpdated: React.PropTypes.func.isRequired
};


/**
 * Component header of table.
 */
class TableHead extends Component {

    /**
     * @returns {XML} Header of table.
     */
    render() {
        return (
            <thead>{this.generateHeader()}</thead>
        );
    }

    /**
     * @returns {XML} Header rows of table.
     */
    generateHeader() {
        var cells = this.props.header.map(function (colData) {
            return <th key={colData.key}>{colData.label}</th>;
        });

        return (<tr>{cells}</tr>);
    }
}
TableHead.propTypes = {header: React.PropTypes.array.isRequired};


/**
 * Component body of table.
 *
 * Drobny ustupek.
 * Dalsi rozpadani (na radky), uz zneprehlednuje kod.
 */
class TableBody extends Component {

    /**
     * @returns {XML} Body of table.
     */
    render() {
        return (
            <tbody>{this.generateRows()}</tbody>
        );
    }

    /**
     * @returns {Object|*|Array} Rows of table with cells.
     */
    generateRows() {
        var header = this.props.header;  // [{key, label}]
        var data = this.props.data;    // [{id, firstname, lastname}]
        var onRowDeleted = this.props.onRowDeleted;
        var onRowUpdated = this.props.onRowUpdated;

        return data.map(function (item) {
            var cells = header.map(function (colData) {

                if (colData.key === 'action') {
                    return <td key={item.id}>
                        <BsButton style={"warning"} label={"Edit"} callback={onRowUpdated} itemID={item.id}/>
                        <BsButton style={"danger"} label={"Delete"} callback={onRowDeleted} itemID={item.id}/>
                    </td>;
                } else {
                    var label = item[colData.key].toString();
                    return <Cell label={label} key={item.id + label}/>;
                }

            });

            return <tr key={item.id}>{cells}</tr>;
        });
    }
}
TableBody.propTypes = {
    header: React.PropTypes.array.isRequired,
    data: React.PropTypes.array.isRequired,
    onRowDeleted: React.PropTypes.func.isRequired,
    onRowUpdated: React.PropTypes.func.isRequired
};


/**
 * Component button. Button must have label and callback after click action.
 */
class BsButton extends Component {

    /**
     * @returns {XML} Button element with label.
     */
    render() {
        return (
            <Button bsStyle={this.props.style} bsSize="small" onClick={() => this.props.callback(this.props.itemID)}>
                {this.props.label}
            </Button>
        );
    }
}
BsButton.propTypes = {
    itemID: React.PropTypes.number.isRequired,
    callback: React.PropTypes.func.isRequired,
    label: React.PropTypes.string.isRequired,
    style: React.PropTypes.string.isRequired
};


/**
 * Component cell of table.
 */
class Cell extends Component {

    /**
     * @returns {XML} Cell of table with label.
     */
    render() {
        return (<td>{this.props.label}</td>);
    }
}
Cell.propTypes = {
    label: React.PropTypes.string.isRequired
};