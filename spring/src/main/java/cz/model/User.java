package cz.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.Size;

/**
 * Created by Tomas Kubicek, 2016.
 */
@Entity()
@Table(name = "Users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @NotBlank
    @Size(min = 3)
    private String name;

    /*
    TODO: Dle zadani nemusi mit uzivatel nastavene username a password => neni mozne pouzit zakomentovane anotace.
          Lepsi by bylo si navic udrzovat boolean parametr 'canLogin' pripadne kazdemu uzivateli priradit roli.
          Uzivatelske jmeno a heslo by pak mohlo byt nastaveno na vychozi (not null) hodnotu a 'canLogin' na #false.
     */
    //@NotBlank
    //@Size(min = 3)
    @Column(unique = true)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String username;

    //@NotBlank
    //@Size(min = 6)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;

    /*
    Constructors
     */
    public User() {
    }

    public User(String name) {
        this.name = name;
    }

    /*
    Methods
     */
    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    /*
    Getters And Setters
     */
    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
