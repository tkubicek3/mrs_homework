package cz.controller;

import cz.model.User;
import cz.model.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by Tomas Kubicek, 2016.
 */
@CrossOrigin
@RestController
public class UserController {

    @Autowired
    private UserRepository repository;

    @Autowired
    private PasswordEncoder passwordEncoder;


    /**
     * Find and return User by his ID.
     *
     * @param
     * @return Existed entity of User.
     */
    @RequestMapping(path = "/api/user", method = RequestMethod.GET)
    public ResponseEntity<?> getUserById(@RequestParam long id) {
        User user;

        try {
            user = repository.getUserById(id);

            if (user == null)
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("User with id: " + id + " not exist.");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal server error in getUserById.");
        }

        return ResponseEntity.ok(user);
    }


    /**
     * Return all Users who are stored in the database.
     *
     * @param
     * @return Existed entity of User.
     */
    @RequestMapping(path = "/api/users", method = RequestMethod.GET)
    public ResponseEntity<?> getAllUsers() {
        List<User> users;

        try {
            users = repository.findAllByOrderById();
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal server error in getAllUsers.");
        }

        return ResponseEntity.ok(users);
    }


    /**
     * Insert new User to database.
     *
     * @param user JSON with new user. Name is required.
     * @return New entity of User.
     */
    @RequestMapping(path = "/api/user", method = RequestMethod.POST, consumes = "application/json")
    public ResponseEntity<?> createUser(@Valid @RequestBody User user) {

        try {
            user.setId(0);

            // Password hashing
            if (user.getPassword() != null)
                user.setPassword(passwordEncoder.encode(user.getPassword()));

            user = repository.save(user);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal server error in createUser.");
        }

        return ResponseEntity.ok(user);
    }


    /**
     * Update existing User in database.
     *
     * @param user
     * @return
     */
    @RequestMapping(path = "/api/user", method = RequestMethod.PUT, consumes = "application/json")
    public ResponseEntity<?> updateUser(@Valid @RequestBody User user) {
        try {
            User oldUser = repository.findOne(user.getId());

            if (oldUser != null) {
                // Password hashing
                if (user.getPassword() != null)
                    oldUser.setPassword(passwordEncoder.encode(user.getPassword()));

                if (user.getUsername() != null)
                    oldUser.setUsername(user.getUsername());

                oldUser.setName(user.getName());

                user = repository.save(oldUser);
            } else {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("User with id: " + user.getId() + " not exist.");
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal server error in updateUser.");
        }

        return ResponseEntity.ok(user);
    }
}