package cz.controller;

import cz.model.User;
import cz.model.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Created by Tomas Kubicek, 2016.
 */
@CrossOrigin
@RestController
public class UserSecureController {

    @Autowired
    private UserRepository repository;


    /**
     * Delete existing User from database.
     *
     * @param user
     * @return
     */
    @RequestMapping(path = "/api/secured/user", method = RequestMethod.DELETE, consumes = "application/json")
    public ResponseEntity<String> deleteUser(@Valid @RequestBody User user) {

        try {
            repository.delete(user.getId());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("User with id: " + user.getId() + " not exist.");
        }

        return ResponseEntity.ok("User with id: " + user.getId() + " deleted successfully.");
    }
}
